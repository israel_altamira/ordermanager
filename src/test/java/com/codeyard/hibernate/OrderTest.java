package com.codeyard.hibernate;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.codeyard.order.Order;

public class OrderTest {
	SessionFactory sessionFactory;

	@Before
	public void init() {
		try {
			sessionFactory = new Configuration().configure().buildSessionFactory();
		} catch (Throwable ex) {
			System.err.println("Failed to create session factory." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}
	
	@After
	public void after() {
		sessionFactory.close();
	}
	
	@Test
	public void testOrderCreation(){
		
		Session session = sessionFactory.openSession();
		
		session.beginTransaction();
		Order order = new Order();
 
		order.setOrderId(6);
		order.setOrderDate(new Date());
		order.setCustomerName("Richa");
		order.setOrderType("Small");
 
		session.save(order);
		session.getTransaction().commit();
		System.out.println("Row added");
	}


}
