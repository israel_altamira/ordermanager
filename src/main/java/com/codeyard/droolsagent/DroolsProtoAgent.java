package com.codeyard.droolsagent;

import org.drools.RuleBase;
import org.drools.StatefulSession;
import org.drools.agent.RuleAgent;

public class DroolsProtoAgent {
	private RuleBase rulebase;
	private StatefulSession session;

	private DroolsProtoAgent() {
	}

	public RuleBase getRulebase() {
		if (rulebase != null)
			return rulebase;
		else {
			RuleAgent agent = RuleAgent
					.newRuleAgent("/brmsdeployedrules.properties");
			RuleBase rulebase = agent.getRuleBase();
			return rulebase;
		}
	}

	public void setRulebase(RuleBase rulebase) {
		this.rulebase = rulebase;
	}

	public void setSession(StatefulSession session) {
		this.session = session;
	}

	public StatefulSession getSession() {
		if (rulebase == null) {
			rulebase = getRulebase();
		}
		session = rulebase.newStatefulSession();
		return session;
	}
}
