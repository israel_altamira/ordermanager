package com.codeyard.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.codeyard.model.AdvanceOrder;

@Repository
public class OrderImpl implements OrderDao {

	@Autowired
	private SessionFactory sessionFactory;

	public void createOrder(AdvanceOrder order) {
		//order.setOrderDate(new Date());
		sessionFactory.getCurrentSession().save(order);
	}

	@SuppressWarnings("unchecked")
	public List<AdvanceOrder> getAllOrders() {
		return sessionFactory.getCurrentSession()
				.createQuery("from AdvanceOrder").list();
	}

	public void deleteOrder(Integer id) {
		AdvanceOrder order = (AdvanceOrder) sessionFactory.getCurrentSession()
				.load(AdvanceOrder.class, id);
		if (null != order) {
			sessionFactory.getCurrentSession().delete(order);
		}
	}

	public boolean saveOrders(List<AdvanceOrder> orderList) {
		System.out.println("Guardar conjunto de Orders");
		for (AdvanceOrder order : orderList) {
			sessionFactory.getCurrentSession().merge(order);
			System.out.println("Se guardo order: " + order.getOrderId());
		}
		return true;
	}

}
