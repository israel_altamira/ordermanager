package com.codeyard.dao;

import java.util.List;

import com.codeyard.model.AdvanceOrder;

public interface OrderDao {

	public void createOrder(AdvanceOrder order);

	public List<AdvanceOrder> getAllOrders();

	public void deleteOrder(Integer id);

	public boolean saveOrders(List<AdvanceOrder> orderList);
	
}
