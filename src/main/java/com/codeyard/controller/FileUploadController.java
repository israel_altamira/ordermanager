package com.codeyard.controller;

import java.io.File;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.codeyard.service.FileKeeperService;

@Controller
@RequestMapping("/uploadform")
public class FileUploadController {

	@Autowired
	private FileKeeperService fileKeeperService;

	@RequestMapping(method = RequestMethod.GET)
	public String fileUpload(HttpServletRequest request) throws Exception {
		return "uploadform";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String handleFileUpload(HttpServletRequest request,
			@RequestParam CommonsMultipartFile[] fileUpload) throws Exception {

		System.out.println("description: "
				+ request.getParameter("description"));

		if (fileUpload != null && fileUpload.length > 0) {

			//fileKeeperService.clearDeposit();

			for (CommonsMultipartFile aFile : fileUpload) {

				System.out.println("Saving file: "
						+ aFile.getOriginalFilename());

				if (!aFile.getOriginalFilename().equals("")) {
					File toSave = new File(FileKeeperService.SAVE_DIRECTORY 
							+ aFile.getOriginalFilename());
					aFile.transferTo(toSave);
					boolean flag = fileKeeperService.addFile4Save(toSave);
					System.out.println("It was saved? " + flag);
				}
			}
		}

		return "result";
	}

}