package com.codeyard.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.codeyard.model.AdvanceOrder;
import com.codeyard.service.BussinesRulesService;
import com.codeyard.service.OrderService;

@Controller
@RequestMapping("/orders")
public class OrdersController {

	@Autowired
	private OrderService orderService;

	@Autowired
	private BussinesRulesService businessRulesService;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String listContacts(Map<String, Object> map) {

		map.put("order", new AdvanceOrder());
		map.put("orderList", orderService.getAllOrders());

		return "order";
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String addContact(@ModelAttribute("order") AdvanceOrder order,
			BindingResult result) {
		orderService.createOrder(order);
		return "redirect:/orders/list";
	}

	@RequestMapping("/delete/{orderId}")
	public String deleteContact(@PathVariable("orderId") Integer orderId) {
		orderService.deleteOrder(orderId);
		return "redirect:/orders/list";
	}

	@RequestMapping("/applyrules/{guvnorFlag}")
	public String processDatabase(@PathVariable("guvnorFlag") boolean guvnorFlag) {
		if(guvnorFlag){
			businessRulesService.processDatabaseWithGuvnor();
		} else {
			businessRulesService.processDatabase();
		}
		
		return "redirect:/orders/list";
	}

}