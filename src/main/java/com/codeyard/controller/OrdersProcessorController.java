package com.codeyard.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.codeyard.model.AdvanceOrder;
import com.codeyard.service.FileKeeperService;
import com.codeyard.service.OrderService;

@Controller
@RequestMapping("/process")
public class OrdersProcessorController {

	private Logger LOGGER = LoggerFactory
			.getLogger(OrdersProcessorController.class);

	@Autowired
	private OrderService orderService;

	@Autowired
	private FileKeeperService fileKeeperService;

	/**
	 * This method show all uploaded files that file-service has registered
	 * 
	 * @param map
	 * @param request
	 * @param poiSuccess
	 *            Process view allow to go and process a file with POI lib. If
	 *            it was ok, this method receives this success flag
	 * @return
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String listContacts(Map<String, Object> map,
			HttpServletRequest request,
			@RequestParam(required = false) boolean poiSuccess) {

		System.out.println("Archivos existentes:");
		for (File file : fileKeeperService.getAllFiles()) {
			System.out.println(file.getName());
		}

		map.put("fileList", fileKeeperService.getAllFiles());

		if (poiSuccess) {
			map.put("poiSuccessMsg", "File was sucessfully processed!");
		}

		return "process";
	}

	/**
	 * @description Se lee el archivo seleccionado en la UI con POI y se guarda
	 *              lo encontrado en la BBDD
	 * 
	 */
	@RequestMapping(value = "/do", method = RequestMethod.GET)
	public String handleFileUpload(HttpServletRequest request) throws Exception {
		// @PathVariable("filename") String fileName

		String fileName = request.getParameter("fileName");
		System.out.println("Procesando archivo: " + fileName);

		try {

			FileInputStream file = new FileInputStream(fileKeeperService
					.getFile(fileName).getAbsolutePath());
			System.out.println("SE ENCONTRO ARCHIVO...");

			// Get the workbook instance for XLS file
			HSSFWorkbook workbook = new HSSFWorkbook(file);

			// Get first sheet from the workbook
			HSSFSheet sheet = workbook.getSheetAt(0);

			// Iterate through each rows from first sheet
			Iterator<Row> rowIterator = sheet.iterator();
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();

				// For each row, iterate through each columns
				Iterator<Cell> cellIterator = row.cellIterator();
				AdvanceOrder order = new AdvanceOrder();
				while (cellIterator.hasNext()) {

					//

					Cell cell = cellIterator.next();

					if (cell.getRow().getRowNum() == 0) {
					} else {
						//

						if (cell.getColumnIndex() == 1) {
							order.setCustomerName(cell.getStringCellValue());
						}
						if (cell.getColumnIndex() == 2) {
							order.setOrderType(cell.getStringCellValue());
						}
						if (cell.getColumnIndex() == 3) {
							order.setOrderDate(cell.getDateCellValue());
						}
						if (cell.getColumnIndex() == 4) {
							order.setProduct(cell.getStringCellValue());
						}

						switch (cell.getCellType()) {
						case Cell.CELL_TYPE_BOOLEAN:
							LOGGER.debug(String.format(
									"boolean.cell.value(%s).index(%s)",
									cell.getBooleanCellValue(),
									cell.getColumnIndex()));
							break;
						case Cell.CELL_TYPE_NUMERIC:
							LOGGER.debug(String.format(
									"numeric.cell.value(%s).index(%s)",
									cell.getNumericCellValue(),
									cell.getColumnIndex()));
							break;
						case Cell.CELL_TYPE_STRING:
							LOGGER.debug(String.format(
									"string.cell.value(%s).index(%s)",
									cell.getStringCellValue(),
									cell.getColumnIndex()));
							break;
						}

					}

				}
				if (row.getRowNum() != 0) {
					orderService.createOrder(order);
				}
			}
			file.close();

			FileOutputStream out = new FileOutputStream(new File(
					FileKeeperService.SAVE_DIRECTORY + fileName));
			workbook.write(out);
			System.out.println("Se guardo el archivo modificado satisfactoriamente...");
			out.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return "redirect:/process/list?poiSuccess=true";
	}

}
