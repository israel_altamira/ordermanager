package com.codeyard.service;

import java.util.List;

import com.codeyard.model.AdvanceOrder;

public interface OrderService {

	public void createOrder(AdvanceOrder contact);

	public List<AdvanceOrder> getAllOrders();

	public void deleteOrder(Integer id);

	public boolean saveOrders(List<AdvanceOrder> orderList);
	
}
