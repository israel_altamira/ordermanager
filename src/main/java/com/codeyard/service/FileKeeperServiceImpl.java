package com.codeyard.service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class FileKeeperServiceImpl implements FileKeeperService {

	private List<File> fileList = new ArrayList<File>();
	
	@Override
	public void clearDeposit() {
		System.out.println("Cleaning");
		if(fileList!=null){
			fileList.clear();
		}
		System.out.println("Cleaning end > list_size > " + fileList.size());
	}

	@Override
	public boolean addFile4Save(File file) {
		if(fileList!=null){
			System.out.println("service > adding file > " + file.getName());
			fileList.add(file);
			return true;
		}
		return false;
	}

	@Override
	public List<File> getAllFiles() {
		return fileList;
	}

	@Override
	public File getFile(String name) {
		if(null!=name && fileList!=null){
			for(File file: fileList){
				if(name.equalsIgnoreCase(file.getName())){
					return file;
				}
			}
		}
		return null;
	}


}
