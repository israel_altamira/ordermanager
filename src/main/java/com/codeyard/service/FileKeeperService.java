package com.codeyard.service;

import java.io.File;
import java.util.List;

public interface FileKeeperService {

	public String SAVE_DIRECTORY = "C:\\_FLOTILLAS\\_temp\\ordermanager\\workspace\\";
	
	public void clearDeposit();
	
	public boolean addFile4Save(File file);
	
	public List<File> getAllFiles();
	
	public File getFile(String string);
	
}
