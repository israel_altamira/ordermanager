package com.codeyard.service;

import java.util.List;

import com.codeyard.model.AdvanceOrder;

public interface RuleDroolService {

	public String applyRule(List<AdvanceOrder> orders);
	
}
