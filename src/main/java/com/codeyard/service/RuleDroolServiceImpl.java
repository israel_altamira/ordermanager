package com.codeyard.service;

import java.util.List;

import org.drools.StatefulSession;
import org.springframework.beans.factory.annotation.Autowired;

import com.codeyard.droolsagent.DroolsProtoAgent;
import com.codeyard.model.AdvanceOrder;

public class RuleDroolServiceImpl implements RuleDroolService {

	@Autowired
	private DroolsProtoAgent ruleAgent;

	@Override
	public String applyRule(List<AdvanceOrder> orders) {

		try {
			// Load knowledge base session
			StatefulSession ksession = ruleAgent.getSession();

			// Start a predefined process in guvnor
			// ksession.startProcess("droolsProcess");

			ksession.insert(orders);

			System.out.println("Applying rules...");
			ksession.fireAllRules(); // Apply rules
			return "success or specific guvnor response";
		} catch (Throwable t) {
			t.printStackTrace();
			return "failure";
		}
	}

	public DroolsProtoAgent getRuleAgent() {
		return ruleAgent;
	}

	public void setRuleAgent(DroolsProtoAgent ruleAgent) {
		this.ruleAgent = ruleAgent;
	}

}
