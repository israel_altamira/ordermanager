package com.codeyard.service;


public interface BussinesRulesService {

	public void processDatabase();
	
	public void processDatabaseWithGuvnor();
	
}
