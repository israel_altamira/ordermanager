package com.codeyard.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.codeyard.dao.OrderDao;
import com.codeyard.model.AdvanceOrder;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	private OrderDao orderDAO;

	@Transactional
	public void createOrder(AdvanceOrder contact) {
		orderDAO.createOrder(contact);
	}

	@Transactional
	public List<AdvanceOrder> getAllOrders() {
		return orderDAO.getAllOrders();
	}

	@Transactional
	public void deleteOrder(Integer id) {
		orderDAO.deleteOrder(id);
	}

	@Transactional
	public boolean saveOrders(List<AdvanceOrder> orderList) {
		return orderDAO.saveOrders(orderList);
	}

}
