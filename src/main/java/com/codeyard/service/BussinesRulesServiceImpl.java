package com.codeyard.service;

import java.util.List;

import org.drools.runtime.StatelessKnowledgeSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.codeyard.model.AdvanceOrder;

@Service
public class BussinesRulesServiceImpl implements BussinesRulesService {

	@Autowired
	@Qualifier("ordersKSession")
	StatelessKnowledgeSession ksession;
	
	@Autowired
	OrderService orderService;
	
	@Autowired
	RuleDroolService droolService;
	
	@Override
	public void processDatabase() {
		System.out.println("Executing rules over registres...");
		List<AdvanceOrder> orders = orderService.getAllOrders();
		System.out.println(String.format("Se van a procesar %s registros", orders.size()));
		ksession.execute(orders);
		orderService.saveOrders(orders);
	}

	@Override
	public void processDatabaseWithGuvnor() {
		System.out.println("Executing rules over registres...");
		List<AdvanceOrder> orders = orderService.getAllOrders();
		System.out.println(String.format("Se van a procesar %s registros", orders.size()));
		System.out.println("Result of applyied rules: " + droolService.applyRule(orders) );
		orderService.saveOrders(orders);
	}
	
}
