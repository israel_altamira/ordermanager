<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Spring Hibernate Sample Orders</title>
</head>
<c:set var="context" value="${pageContext.request.contextPath}" />
<body>
	<center>
	<h2>Log Order Details</h2>

	<form:form method="post" action="save.html" commandName="order">

		<table>
			<tr>
				<td><form:label path="customerName">Customer Name</form:label></td>
				<td><form:input path="customerName" /></td>
			</tr>
			<tr>
				<td><form:label path="OrderType">Order Type</form:label></td>
				<td><form:input path="OrderType" /></td>
			</tr>
			<tr>
				<td><form:label path="product">Product</form:label></td>
				<td><form:input path="product" /></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="Add Order" /></td>
			</tr>
		</table>
	</form:form>


	<h3>Order History</h3>
	<c:if test="${!empty orderList}">
		<table border="1">

			<th>Customer Name</th>
			<th>Product</th>
			<th>Order Date</th>
			<th>Order Type</th>

			<c:forEach items="${orderList}" var="order">
				<tr>
					<td>${order.customerName}</td>
					<td>${order.product}</td>
					<td>${order.orderDate}</td>
					<td>${order.orderType}</td>
					<td><a href="${context}/delete/${order.orderId}">delete</a></td>
				</tr>
			</c:forEach>
		</table>
	</c:if>
	
	<p>
		<span><a href="${context}/uploadform">Go to upload a file</a></span><br />
		<span><a href="${context}/process/list">Show my files</a></span><br />
		<span><a href="${context}/orders/applyrules/true">Apply GUVNOR-DROOLs rules to DDBB</a></span><br />
		<span><a href="${context}/orders/applyrules/false">Apply DROOLs rules to DDBB</a></span>
	</p>
	
	</center>
</body>
</html>
