<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Error</title>
</head>
<c:set var="context" value="${pageContext.request.contextPath}"></c:set>
<body>
	<center>
		<h2>Sorry, there was an error occurred:</h3>
		<h3>${exception.message}</h2>
		<p>
			Go to home (list of orders page) <a href="${context}/orders/list">orders</a>
		</p>
	</center>
</body>
</html>