<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.io.File" %>
<%@ page import="java.util.List" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Spring MVC File Upload Demo</title>
</head>
<c:set var="context" value="${pageContext.request.contextPath}" />
<body>
	<center>
		<h1>Process Files</h1>

		<c:if test="${!empty poiSuccessMsg}">
		<p>${poiSuccessMsg}</p>	
		</c:if>
		
		<c:if test="${!empty fileList}">
			<table border="1">
				<tr>
					<th>List of available Files</th>
				</tr>
				
				<c:forEach items="${requestScope.fileList}" var="myfile">
					<tr>
						<td>${myfile.name}</td>
						<td><a href="${context}/process/do?fileName=${myfile.name}">process with POI</a></td>
					</tr>
				</c:forEach>
				
			</table>
		</c:if>

		<p>
			Go to see list of <a href="${context}/orders/list">orders</a>
		</p>
	</center>

</body>
</html>
